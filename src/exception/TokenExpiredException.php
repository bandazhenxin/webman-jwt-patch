<?php


namespace bdhert\JwtAuth\exception;

/**
 * Token 已过期.
 */
class TokenExpiredException extends JwtException
{
}
