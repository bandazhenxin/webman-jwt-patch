<?php
namespace bdhert\JwtAuth\exception;

/**
 * 内部依赖错误
 * Class RelyException
 * @package bdhert\JwtAuth\exception
 */
class RelyException extends JwtException {}